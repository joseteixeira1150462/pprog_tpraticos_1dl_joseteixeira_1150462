package clubedesportivo;

/**
 *
 * @author Jose Paulo da Silva Teixeira 1150462
 */
public class Main {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {

        AtletaProfissional ap1 = new AtletaProfissional("Joao", "123456", "Masculino", 21, "Caminhada", 130, 80, 0.5f, 40, 80, 150, 0.3f);
        AtletaProfissional ap2 = new AtletaProfissional("Ana", "223456", "F", 20, "Natacao", 15, 12, 0.32f, 60, 70, 220, 0.41f);

        AtletaSemiprofissional asp1 = new AtletaSemiprofissional("Rita", "323456", "Feminino", 26, "Natacao", 110, 87, 0.25f, 107, 0, 25f, 10);
        AtletaSemiprofissional asp2 = new AtletaSemiprofissional("Pedro", "423456", "M", 28, "Corrida", 150, 113, 0.3f, 50, 50, 45f, 5);
        AtletaSemiprofissional asp3 = new AtletaSemiprofissional("Bruno", "523456", "Masculino", 18, "Ciclismo", 169, 90, 0.3f, 50, 60, 95f, 21);

        AtletaAmador aa1 = new AtletaAmador("Francisca", "623456", "Feminino", 22, "Corrida", 141, 112, 0.21f, 110, 40, 15f, 2);
        AtletaAmador aa2 = new AtletaAmador("Andre", "723456", "M", 21, "Natacao", 181, 120, 0.29f, 40, 50, 8f, 1);

        ListaAtletas lista = new ListaAtletas();
        ClubeDesportivo cd = new ClubeDesportivo("Clube Desportivo A", new Data(2015, 05, 02), lista);

        cd.getListaAtletas().addAtleta(ap1);
        cd.getListaAtletas().addAtleta(ap2);
        cd.getListaAtletas().addAtleta(asp1);
        cd.getListaAtletas().addAtleta(asp2);
        cd.getListaAtletas().addAtleta(asp3);
        cd.getListaAtletas().addAtleta(aa1);
        cd.getListaAtletas().addAtleta(aa2);
        System.out.println("                1ª iteração");
        for (Atleta a : cd.getListaAtletas().getLista()) {
            if (a instanceof AtletaSemiprofissional) {
                System.out.println("Atleta Semiprofissional\nNome: " + a.getNome() + "\n"
                        + "Frequencia Cardiaca Maxima: " + a.calcularFCM() + "\n"
                        + "Frequencia Cardiaca de Trabalho: " + a.getFct() + "\n");
            }
            if (a instanceof AtletaAmador) {
                System.out.println("Atleta Amador\nNome: " + a.getNome() + "\n"
                        + "Frequencia Cardiaca Maxima: " + a.calcularFCM() + "\n"
                        + "Frequencia Cardiaca de Trabalho: " + a.getFct() + "\n");

            }

        }
        for (int i = 0; i < cd.getListaAtletas().getLista().size(); i++) {
            System.out.println("\nNome: " + cd.getListaAtletas().getLista().get(i).getNome() + "\n"
                    + "Valor a pagar: " + cd.getListaAtletas().getLista().get(i).calcularSalario() + "€\n");
        }

        System.out.println("\nTotal de atletas amadores: " + aa2.getContador());

        System.out.println("\nTotal de atletas profissionais: " + ap2.getContador());

        System.out.println("========================================================");
        System.out.println("                2ª iteração");

        System.out.println("Nome do Clube: " + cd.getNome());

        AtletaAmador aa3 = new AtletaAmador("Filipe", "723456", "Masculino", 21, "Corrida", 141, 112, 0.21f, 110, 40, 18f, 3);
        cd.getListaAtletas().getLista().add(aa3);
        int actualSize = cd.getListaAtletas().getLista().size() - 1; // ultimo valor do array
        System.out.println("\nO " + cd.getListaAtletas().getLista().get(actualSize).getNome() + " foi adicionionado a lista");

        ClubeDesportivo.ordenarAlfabeticamentePorNome(cd.getListaAtletas().getLista());

        System.out.println("\nLista de Nomes dos Atletas Ordenada Alfabeticamente:");
        for (int i = 0; i < cd.getListaAtletas().getLista().size(); i++) {
            System.out.println(cd.getListaAtletas().getLista().get(i).getNome());
        }

        ClubeDesportivo.ordenarInversamentePorValorPremiosMensal(cd.getListaAtletas().getLista());
        System.out.println("\nLista de valores de prémios acumulados dos Atletas por ordem inversa");
        float totalDescontoIRSPro = 0f;
        float totalDescontoIRSSemiPro = 0f;
        for (int i = 0; i < cd.getListaAtletas().getLista().size(); i++) {
            System.out.println(cd.getListaAtletas().getLista().get(i).getValorQntPremiosMensal() + "€");
        }
        for (Atleta a : cd.getListaAtletas().getLista()) {
            if (a instanceof AtletaProfissional) {
                totalDescontoIRSPro += totalDescontoIRSPro + ((AtletaProfissional) a).calcularDescontoIRS();

                if (a instanceof AtletaSemiprofissional) {
                    totalDescontoIRSSemiPro += totalDescontoIRSSemiPro + ((AtletaSemiprofissional) a).calcularDescontoIRS();
                }

            }

        }
        float totalDescontoIRS = totalDescontoIRSPro + totalDescontoIRSSemiPro;
        System.out.println("\nO total do desconto de cada atleta Pro/Semipro é " + totalDescontoIRS + "€");

        System.out.println("\nLista de Nomes dos Atletas Ordenada Alfabeticamentem por Nome e por Categoria/Modalidade: ");

        ClubeDesportivo.ordenarAlfabeticamentePorNome(cd.getListaAtletas().getLista());
        ClubeDesportivo.ordenarAlfabeticamentePorAtividade(cd.getListaAtletas().getLista());

        for (int i = 0; i < cd.getListaAtletas().getLista().size(); i++) {
            System.out.println("Nome: " + cd.getListaAtletas().getLista().get(i).getNome() + " *** Categoria: " + cd.getListaAtletas().getLista().get(i).getAtividade());
        }
    }

}
