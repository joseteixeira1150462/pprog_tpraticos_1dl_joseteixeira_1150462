package clubedesportivo;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Jose Paulo da Silva Teixeira 1150462
 */
public class ClubeDesportivo {

    private String nome;
    private Data dataFormacao;
    private ListaAtletas listaAtletas;

    private final String NOME_POR_OMISSAO = "sem nome";
    private final Data DATA_FORMACAO_POR_OMISSAO = Data.dataAtual();
    private final ListaAtletas LISTA_ATELTAS_POR_OMISSAO = new ListaAtletas();

    public ClubeDesportivo() {
        this.nome = NOME_POR_OMISSAO;
        this.dataFormacao = DATA_FORMACAO_POR_OMISSAO;
        this.listaAtletas = LISTA_ATELTAS_POR_OMISSAO;
    }

    public ClubeDesportivo(String nome, Data dataFormacao, ListaAtletas listaAtletas) {
        this.nome = nome;
        this.dataFormacao = dataFormacao;
        this.listaAtletas = listaAtletas;
    }

    public String getNome() {
        return nome;
    }

    public Data getDataFormacao() {
        return dataFormacao;
    }

    public ListaAtletas getListaAtletas() {
        return listaAtletas;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setDataFormacao(Data dataFormacao) {
        this.dataFormacao = dataFormacao;
    }

    public void setListaAtletas(ListaAtletas listaAtletas) {
        this.listaAtletas = listaAtletas;
    }

    /**
     * Método que permite ordenar inversamento por valor de premios mensal de um
     * Atleta.
     *
     * @param lista
     */
    public static void ordenarInversamentePorValorPremiosMensal(List<Atleta> lista) {

        Collections.sort(lista, new Comparator() {

            @Override
            public int compare(Object a1, Object a2) {
                int valorQntPremios1 = (int) ((Atleta) a1).getValorQntPremiosMensal();
                int valorQntPremios2 = (int) ((Atleta) a2).getValorQntPremiosMensal();

                if (valorQntPremios1 > valorQntPremios2) {
                    return -1;
                } else if (valorQntPremios1 < valorQntPremios2) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });
    }

    /**
     * Método que permite ordenar alfabeticamente por nome, um Atleta.
     *
     * @param lista
     */
    public static void ordenarAlfabeticamentePorNome(List<Atleta> lista) {

        Collections.sort(lista, new Comparator() {

            @Override
            public int compare(Object a1, Object a2) {
                String nome1 = ((Atleta) a1).getNome();
                String nome2 = ((Atleta) a2).getNome();
                return nome1.compareTo(nome2);
            }
        });
    }

    /**
     * Método que permite ordenar alfabeticamente por Modalidade, um Atleta.
     *
     * @param lista
     */
    public static void ordenarAlfabeticamentePorAtividade(List<Atleta> lista) {

        Collections.sort(lista, new Comparator() {

            @Override
            public int compare(Object a1, Object a2) {
                String atividade1 = ((Atleta) a1).getAtividade();
                String atividade2 = ((Atleta) a2).getAtividade();
                return atividade1.compareTo(atividade2);
            }
        });
    }

    @Override
    public String toString() {
        return "Clube Desportivo: \nNome: " + this.getNome() + "\nData de Formaçao: " + this.dataFormacao.toString() + "\nLista de Atletas: " + this.listaAtletas.toString();
    }

}
