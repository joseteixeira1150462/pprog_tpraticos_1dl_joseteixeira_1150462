package clubedesportivo;

/**
 *
 * @author Jose Paulo da Silva Teixeira 1150462
 */
public abstract class AtletaNaoProfissional extends Atleta {

    private int anosClube;

    private final int ANOS_CLUBE_POR_OMISSAO = 0;

    /**
     *
     */
    public AtletaNaoProfissional() {
        super();
        this.anosClube = ANOS_CLUBE_POR_OMISSAO;
        this.setContador(this.getContador() + 1);
    }

    /**
     *
     * @param nome
     * @param numID
     * @param genero
     * @param idade
     * @param atividade
     * @param fct
     * @param fcr
     * @param percentagemIT
     * @param parcelaFixa
     * @param parcelaVariavel
     * @param valorQntPremios
     * @param anosClube
     */
    public AtletaNaoProfissional(String nome, String numID, String genero, int idade, String atividade, float fct, float fcr, float percentagemIT, float parcelaFixa, float parcelaVariavel, float valorQntPremios, int anosClube) {
        super(nome, numID, genero, idade, atividade, fct, fcr, percentagemIT, parcelaFixa, parcelaVariavel, valorQntPremios);
        this.anosClube = anosClube;
        this.setContador(this.getContador() + 1);
    }

    /**
     * Retorna o numero de anos que o atleta tem no clube
     *
     * @return anosClube
     */
    public int getAnosClube() {
        return anosClube;
    }

    /**
     * Altera o valor de anos que o atleta tem no clube
     *
     * @param anosClube
     */
    public void setAnosClube(int anosClube) {
        this.anosClube = anosClube;
    }

    /**
     * Calcula a Parcela Variavel em função dos anos e da parcela fixa.
     */
    @Override
    public void calcularParcelaVariavel() {
        if (this.getAnosClube() >= 5 && this.getAnosClube() <= 10) {
            this.setParcelaVariavel(this.getParcelaFixa() + (0.02f * this.getParcelaFixa()));
        } else if (this.getAnosClube() > 10 && this.getAnosClube() <= 20) {
            this.setParcelaVariavel(this.getParcelaFixa() + (0.08f * this.getParcelaFixa()));
        } else {
            this.setParcelaVariavel(this.getParcelaFixa() + (0.2f * this.getParcelaFixa()));
        }
    }

    /**
     * Retorna o valor do salario do atleta
     *
     * @return parcelaVariavel + parcelaFixa
     */
    @Override
    public abstract float calcularSalario();
        

    @Override
    public String toString() {
        return super.toString() + "\nAnos de Clube: " + this.getAnosClube() + "\n";
    }

}
