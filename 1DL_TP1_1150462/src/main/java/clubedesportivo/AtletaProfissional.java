package clubedesportivo;

/**
 *
 * @author Jose Paulo da Silva Teixeira 1150462
 */
public class AtletaProfissional extends Atleta implements IRS {

    private float percentagemParcelaVariavel;
    private final float descontoIRS;
    private final float PERCENTAGEM_PARCELA_VARIAVEL_POR_OMISSAO = 0.2f;

    /**
     *
     */
    public AtletaProfissional() {
        super();
        this.percentagemParcelaVariavel = PERCENTAGEM_PARCELA_VARIAVEL_POR_OMISSAO;
        this.descontoIRS = 0.10f;
        this.setContador(this.getContador() + 1);
    }

    /**
     *
     * @param nome
     * @param numID
     * @param genero
     * @param idade
     * @param atividade
     * @param fct
     * @param fcr
     * @param percentagemIT
     * @param parcelaFixa
     * @param parcelaVariavel
     * @param valorQntPremios
     * @param percentagemParcelaVariavel
     */
    public AtletaProfissional(String nome, String numID, String genero, int idade, String atividade, float fct, float fcr, float percentagemIT, float parcelaFixa, float parcelaVariavel, float valorQntPremios, float percentagemParcelaVariavel) {
        super(nome, numID, genero, idade, atividade, fct, fcr, percentagemIT, parcelaFixa, parcelaVariavel, valorQntPremios);
        this.percentagemParcelaVariavel = percentagemParcelaVariavel;
        this.descontoIRS = 0.10f;
        this.setContador(this.getContador() + 1);
    }

    /**
     *
     * @return
     */
    public float getPercentagemParcelaVariavel() {
        return percentagemParcelaVariavel;
    }

    public float getDescontoIRS() {
        return descontoIRS;
    }

    /**
     *
     * @param percentagemParcelaVariavel
     */
    public void setPercentagemParcelaVariavel(float percentagemParcelaVariavel) {
        this.percentagemParcelaVariavel = percentagemParcelaVariavel;

    }

    /**
     *
     * @param valorQntPremiosMensal
     */
    /**
     *
     */
    public void calcularParcelaVariavelPorOmissao() {
        this.setParcelaVariavel(this.getParcelaVariavel() + (PERCENTAGEM_PARCELA_VARIAVEL_POR_OMISSAO * this.getValorQntPremiosMensal()));
    }

    /**
     *
     */
    @Override
    public void calcularParcelaVariavel() {
        this.setPercentagemParcelaVariavel(this.getParcelaVariavel() + (this.getPercentagemParcelaVariavel() * this.getValorQntPremiosMensal()));
    }

    /**
     *
     * @return
     */
    public float valorParcelaVariavelPorOmissao() {
        calcularParcelaVariavelPorOmissao();
        return this.getParcelaVariavel();
    }

    /**
     *
     * @return
     */
    public float valorParcelaVariavel() {
        calcularParcelaVariavel();
        return this.getParcelaVariavel();
    }

    /**
     *
     * @return
     */
    @Override
    public float calcularSalario() {
        return this.valorParcelaVariavel() + this.calcularDescontoIRS();
    }

    @Override
    public String toString() {
        return "Atleta Profissional " + super.toString()
                + "A percentagem variavel por omissão é " + this.PERCENTAGEM_PARCELA_VARIAVEL_POR_OMISSAO + "%\n"
                + "Valor da Parcela Fixa: " + this.getParcelaFixa() + "€\n"
                + "Valor da Parcela Variavel a " + this.PERCENTAGEM_PARCELA_VARIAVEL_POR_OMISSAO * 100 + "% de percentagem (por omissao): " + this.valorParcelaVariavelPorOmissao() + "€\n"
                + "Valor de Parcela Variavel a " + this.getPercentagemParcelaVariavel() * 100 + "% de percentagem: " + this.valorParcelaVariavel() + "€\n"
                + "Desconto IRS: " + this.descontoIRS * 100 + "%";
    }

    @Override
    public float calcularDescontoIRS() {
        return this.getParcelaFixa() + (this.getDescontoIRS() * this.getParcelaFixa());

    }

}
