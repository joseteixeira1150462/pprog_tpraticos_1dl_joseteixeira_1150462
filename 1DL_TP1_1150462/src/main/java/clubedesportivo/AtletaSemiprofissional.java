package clubedesportivo;

/**
 *
 * @author Jose Paulo da Silva Teixeira 1150462
 */
public class AtletaSemiprofissional extends AtletaNaoProfissional implements IRS {

    private final float descontoIRS;

    public AtletaSemiprofissional() {
        super();
        this.descontoIRS = 0.10f;
        this.setContador(this.getContador() + 1);
    }

    /**
     *
     * @param nome
     * @param numID
     * @param genero
     * @param idade
     * @param atividade
     * @param fct
     * @param fcr
     * @param percentagemIT
     * @param parcelaFixa
     * @param parcelaVariavel
     * @param anosClube
     */
    public AtletaSemiprofissional(String nome, String numID, String genero, int idade, String atividade, float fct, float fcr, float percentagemIT, float parcelaFixa, float parcelaVariavel, float valorQntPremios, int anosClube) {
        super(nome, numID, genero, idade, atividade, fct, fcr, percentagemIT, parcelaFixa, parcelaVariavel, valorQntPremios, anosClube);
        this.descontoIRS = 0.10f;
    }

    public float getDescontoIRS() {
        return descontoIRS;
    }

    @Override
    public String toString() {
        return "Atleta Semiprofissional: " + this.getContador() + ":\n" + super.toString() + "\nDesconto IRS: " + this.descontoIRS * 100 + "%";
    }

    @Override
    public float calcularDescontoIRS() {
        return (this.getParcelaFixa() - (this.getDescontoIRS() * this.getParcelaFixa()));
    }

    @Override
    public float calcularSalario() {
        calcularParcelaVariavel();
        return this.getParcelaVariavel() + this.calcularDescontoIRS();
    }
}
