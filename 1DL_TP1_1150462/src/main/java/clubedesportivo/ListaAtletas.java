package clubedesportivo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jose Paulo da Silva Teixeira 1150462
 */
public class ListaAtletas {

    private List<Atleta> lista = new ArrayList<>();

    /**
     *
     */
    public ListaAtletas() {
        this.lista = new ArrayList<>();
    }

    /**
     *
     * @param a
     */
    public void addAtleta(Atleta a) {
        this.lista.add(a);
    }

    /**
     *
     * @return
     */
    public List<Atleta> getLista() {
        return lista;
    }

    /**
     *
     * @param lista
     */
    public void setLista(List<Atleta> lista) {
        this.lista = lista;
    }

    /**
     *
     * @return
     */
    public int tamanhoLista() {
        return this.getLista().size();
    }
}
