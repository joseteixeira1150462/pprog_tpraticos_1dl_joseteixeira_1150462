package clubedesportivo;

/**
 *
 * @author Jose Paulo da Silva Teixeira 1150462
 */
public abstract class Atleta {

    private String nome;
    private String numID;
    private String genero;
    private int idade;
    private String atividade;
    private float parcelaFixa;
    private float parcelaVariavel;
    private float percentagemIT;
    private float fct;
    private float fcr;
    private float valorQntPremiosMensal;
    private int contador;

    private static final double percentagemITQueimaGordura = 0.6;
    private static final double percentagemITCapacidadeRespiratoria = 0.75;

    private final String NOME_POR_OMISSAO = "s/nome";
    private final String NUM_IDENTIFICACAO_CIVIL_POR_OMISSAO = " ";
    private final String GENERO_POR_OMISSAO = "s/género";
    private final int IDADE_POR_OMISSAO = 0;
    private final String ATIVIDADE_POR_OMISSAO = "s/atividade";
    private final float PARCELA_FIXA_POR_OMISSAO = 0f;
    private final float PARCELA_VARIAVEL_POR_OMISSAO = 0f;
    private final float PERCENTAGEM_IT_POR_OMISSAO = 0f;
    private final float VALOR_QNT_PREMIOS_MENSAL_POR_OMISSAO = 0f;
    private final float FCT_POR_OMISSAO = 0f;
    private final float FCR_POR_OMISSAO = 0f;

    /**
     *
     */
    public Atleta() {
        this.nome = NOME_POR_OMISSAO;
        this.numID = NUM_IDENTIFICACAO_CIVIL_POR_OMISSAO;
        this.genero = GENERO_POR_OMISSAO;
        this.idade = IDADE_POR_OMISSAO;
        this.atividade = ATIVIDADE_POR_OMISSAO;
        this.parcelaFixa = PARCELA_FIXA_POR_OMISSAO;
        this.parcelaVariavel = PARCELA_VARIAVEL_POR_OMISSAO;
        this.percentagemIT = PERCENTAGEM_IT_POR_OMISSAO;
        this.fct = FCT_POR_OMISSAO;
        this.fcr = FCR_POR_OMISSAO;
        this.valorQntPremiosMensal = VALOR_QNT_PREMIOS_MENSAL_POR_OMISSAO;
        this.contador++;

    }

    /**
     *
     * @param nome
     * @param numID
     * @param genero
     * @param idade
     * @param atividade
     * @param fct
     * @param fcr
     * @param percentagemIT
     * @param parcelaFixa
     * @param parcelaVariavel
     */
    public Atleta(String nome, String numID, String genero, int idade, String atividade, float fct, float fcr, float percentagemIT, float parcelaFixa, float parcelaVariavel,float valorQntPremiosMensal) {
        this.nome = nome;
        this.numID = numID;
        this.genero = genero;
        this.idade = idade;
        this.atividade = atividade;
        this.fct = fct;
        this.fcr = fcr;
        this.parcelaFixa = parcelaFixa;
        this.parcelaVariavel = parcelaVariavel;
        this.percentagemIT = percentagemIT;
        this.valorQntPremiosMensal = valorQntPremiosMensal;
        this.contador++;

    }

    /**
     *
     * @return
     */
    public String getNome() {
        return nome;
    }

    /**
     *
     * @return
     */
    public String getNumID() {
        return numID;
    }

    /**
     *
     * @return
     */
    public String getGenero() {
        return genero;
    }

    /**
     *
     * @return
     */
    public int getIdade() {
        return idade;
    }

    /**
     *
     * @return
     */
    public String getAtividade() {
        return atividade;
    }

    /**
     *
     * @return
     */
    public float getPercentagemIT() {
        return percentagemIT;
    }

    /**
     *
     * @return
     */
    public float getParcelaFixa() {
        return parcelaFixa;
    }

    /**
     *
     * @return
     */
    public float getParcelaVariavel() {
        return parcelaVariavel;
    }

    /**
     *
     * @return
     */
    public float getFct() {
        return fct;
    }

    /**
     *
     * @return
     */
    public float getFcr() {
        return fcr;
    }

    /**
     *
     * @return
     */
    public int getContador() {
        return contador;
    }

    public float getValorQntPremiosMensal() {
        return valorQntPremiosMensal;
    }

    /**
     *
     * @return
     */
    public static double getPercentagemITCapacidadeRespiratoria() {
        return percentagemITCapacidadeRespiratoria;
    }

    /**
     *
     * @return
     */
    public static double getPercentagemITQueimaGordura() {
        return percentagemITQueimaGordura;
    }

    /**
     *
     * @param contador
     */
    public void setContador(int contador) {
        this.contador = contador;
    }

    /**
     *
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     *
     * @param numID
     */
    public void setNumID(String numID) {
        this.numID = numID;
    }

    /**
     *
     * @param genero
     */
    public void setGenero(String genero) {
        this.genero = genero;
    }

    /**
     *
     * @param idade
     */
    public void setIdade(int idade) {
        this.idade = idade;
    }

    /**
     *
     * @param atividade
     */
    public void setAtividade(String atividade) {
        this.atividade = atividade;
    }

    /**
     *
     * @param parcelaFixa
     */
    public void setParcelaFixa(float parcelaFixa) {
        this.parcelaFixa = parcelaFixa;
    }

    /**
     *
     * @param parcelaVariavel
     */
    public void setParcelaVariavel(float parcelaVariavel) {
        this.parcelaVariavel = parcelaVariavel;
    }

    /**
     *
     * @param fct
     */
    public void setFct(float fct) {
        this.fct = fct;
    }

    /**
     *
     * @param fct
     */
    public void setFcr(float fct) {
        this.fct = fct;
    }

    /**
     *
     * @param percentagemIT
     */
    public void setPercentagemIT(float percentagemIT) {
        this.percentagemIT = percentagemIT;
    }

    public void setValorQntPremiosMensal(float valorQntPremiosMensal) {
        this.valorQntPremiosMensal = valorQntPremiosMensal;
    }

    /**
     *
     * @return
     */
    public float calcularFCM() {
        float fcm = 0;
        if (this.getAtividade().equalsIgnoreCase("Caminhada") || this.getAtividade().equalsIgnoreCase("Corrida")) {
            if (this.getIdade() > 0) {
                fcm = 208.75f - (0.73f * this.getIdade());
                return fcm;
            }
        }
        if (this.getAtividade().equalsIgnoreCase("Ciclismo")) {
            if (this.getGenero().equalsIgnoreCase("F") || this.getGenero().equalsIgnoreCase("Feminino")) {
                if (this.getIdade() > 0) {
                    fcm = 189 - (0.56f * this.getIdade());
                    return fcm;
                }
            }
        }
        if (this.getGenero().equalsIgnoreCase("M") || this.getGenero().equalsIgnoreCase("Masculino")) {
            if (this.getIdade() > 0) {
                fcm = 202 - (0.72f * this.getIdade());
                return fcm;
            }

        }

        if (this.getAtividade().equalsIgnoreCase("Natacao")) {
            if (this.getIdade() > 0) {
                fcm = 204 - (0.77f * this.getIdade());
                return fcm;
            }

        }
        return fcm;
    }

    /**
     *
     */
    public void calcularFCT() {
        this.setFct(this.getFcr() + (this.getPercentagemIT() * (this.calcularFCM() - this.getFcr())));
    }

    /**
     *
     */
    public abstract void calcularParcelaVariavel();

    /**
     *
     * @return
     */
    public abstract float calcularSalario();

    @Override
    public String toString() {
        return "O Atleta chama-se: " + this.getNome() + "\n"
                + "Numero ID Civil: " + this.getNumID() + "\n"
                + "Genero: " + this.getGenero() + "\n"
                + "Idade: " + this.getIdade() + "\n"
                + "Atividade: " + this.getAtividade() + "\n"
                + "Percentagem de Intensidade de Treino: " + this.getPercentagemIT() * 100 + "%\n"
                + "Frequencia Cardiaca de Trabalho: " + this.getFct() + "\n"
                + "Frequencia Cardia em Repouso: " + this.getFcr() + "\n"
                + "Frequencia Cardiaca Maxima: " + this.calcularFCM() + "\n"
                + "Valor ganho em prémios mensal: " + this.getValorQntPremiosMensal() + "€\n";

    }

}
