package clubedesportivo;

/**
 *
 * @author Jose Paulo da Silva Teixeira 1150462
 */
public class AtletaAmador extends AtletaNaoProfissional {

    /**
     *
     */
    public AtletaAmador() {
        super();
        this.setContador(this.getContador() + 1);
    }

    /**
     *
     * @param nome
     * @param numID
     * @param genero
     * @param idade
     * @param atividade
     * @param fct
     * @param fcr
     * @param percentagemIT
     * @param parcelaFixa
     * @param parcelaVariavel
     * @param valorQntPremios
     * @param anosClube
     */
    public AtletaAmador(String nome, String numID, String genero, int idade, String atividade, float fct, float fcr, float percentagemIT, float parcelaFixa, float parcelaVariavel, float valorQntPremios, int anosClube) {
        super(nome, numID, genero, idade, atividade, fct, fcr, percentagemIT, parcelaFixa, parcelaVariavel, valorQntPremios, anosClube);
    }

    @Override
    public String toString() {
        return "Atleta Amador " + this.getContador() + ":\n" + super.toString();
    }

    @Override
    public float calcularSalario() {
        calcularParcelaVariavel();
        return this.getParcelaVariavel() + this.getParcelaFixa();
    }

}
