/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clubedesportivo;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Zeca Teixeira
 */
public class AtletaNaoProfissionalIT {

    public AtletaNaoProfissionalIT() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getAnosClube method, of class AtletaNaoProfissional.
     */
    @Test
    public void testGetAnosClube() {
        System.out.println("getAnosClube");
        AtletaNaoProfissional instance = new AtletaNaoProfissional() {
            @Override
            public float calcularSalario() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };
        int expResult = 0;
        int result = instance.getAnosClube();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAnosClube method, of class AtletaNaoProfissional.
     */
    @Test
    public void testSetAnosClube() {
        System.out.println("setAnosClube");
        int anosClube = 0;
        AtletaNaoProfissional instance = new AtletaNaoProfissional() {
            @Override
            public float calcularSalario() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };
        instance.setAnosClube(anosClube);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of calcularParcelaVariavel method, of class AtletaNaoProfissional.
     */
    @Test
    public void testCalcularParcelaVariavel() {
        System.out.println("calcularParcelaVariavel");
        AtletaNaoProfissional instance = new AtletaNaoProfissional() {
            @Override
            public float calcularSalario() {
               return 200;
            }
        };
        instance.calcularParcelaVariavel();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class AtletaNaoProfissional.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        AtletaNaoProfissional instance = new AtletaNaoProfissional() {
            @Override
            public float calcularSalario() {
                return 2;
            }
        };
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of calcularSalario method, of class AtletaNaoProfissional.
     */
    @Test
    public void testCalcularSalario() {
        System.out.println("calcularSalario");
        AtletaNaoProfissional instance = new AtletaNaoProfissionalImpl();
        float expResult = 0.0F;
        float result = instance.calcularSalario();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    public class AtletaNaoProfissionalImpl extends AtletaNaoProfissional {

        @Override
        public float calcularSalario() {
            return 200;
        }
    }

}
