package clubedesportivo;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author Jose Paulo da Silva Teixeira 1150462
 */
public class AtletaIT {

    public AtletaIT() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of calcularFCM method, of class Atleta.
     */
    @Test
    public void testCalcularFCM() {
        System.out.println("calcularFCM");
        Atleta instance = new AtletaImpl();
        instance.setAtividade("Caminhada");
        instance.setGenero("F");
        instance.setIdade(22);
        float expResult = 192.69F;
        float result = instance.calcularFCM();
        assertEquals(expResult, result, 0.0F);

        fail("The test case is a prototype.");
    }

    /**
     * Test of calcularFCT method, of class Atleta.
     */
    @Test
    public void testCalcularFCT() {
        System.out.println("calcularFCT");
        Atleta instance = new AtletaImpl();
        instance.calcularFCT();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of calcularParcelaVariavel method, of class Atleta.
     */
    @Test
    public void testCalcularParcelaVariavel() {
        System.out.println("calcularParcelaVariavel");
        Atleta instance = new AtletaImpl();
        instance.calcularParcelaVariavel();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of calcularSalario method, of class Atleta.
     */
    @Test
    public void testCalcularSalario() {
        System.out.println("calcularSalario");
        Atleta instance = new AtletaImpl();
        float expResult = 0.0F;
        float result = instance.calcularSalario();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class Atleta.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Atleta instance = new AtletaImpl();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    public class AtletaImpl extends Atleta {

        @Override
        public void calcularParcelaVariavel() {
        }

        @Override
        public float calcularSalario() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    /**
     * Test of getNome method, of class Atleta.
     */
    @Test
    public void testGetNome() {
        System.out.println("getNome");
        Atleta instance = new AtletaImpl();
        String expResult = "";
        String result = instance.getNome();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNumID method, of class Atleta.
     */
    @Test
    public void testGetNumID() {
        System.out.println("getNumID");
        Atleta instance = new AtletaImpl();
        String expResult = "";
        String result = instance.getNumID();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getGenero method, of class Atleta.
     */
    @Test
    public void testGetGenero() {
        System.out.println("getGenero");
        Atleta instance = new AtletaImpl();
        String expResult = "";
        String result = instance.getGenero();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getIdade method, of class Atleta.
     */
    @Test
    public void testGetIdade() {
        System.out.println("getIdade");
        Atleta instance = new AtletaImpl();
        int expResult = 0;
        int result = instance.getIdade();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAtividade method, of class Atleta.
     */
    @Test
    public void testGetAtividade() {
        System.out.println("getAtividade");
        Atleta instance = new AtletaImpl();
        String expResult = "";
        String result = instance.getAtividade();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getPercentagemIT method, of class Atleta.
     */
    @Test
    public void testGetPercentagemIT() {
        System.out.println("getPercentagemIT");
        Atleta instance = new AtletaImpl();
        float expResult = 0.0F;
        float result = instance.getPercentagemIT();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getParcelaFixa method, of class Atleta.
     */
    @Test
    public void testGetParcelaFixa() {
        System.out.println("getParcelaFixa");
        Atleta instance = new AtletaImpl();
        float expResult = 0.0F;
        float result = instance.getParcelaFixa();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getParcelaVariavel method, of class Atleta.
     */
    @Test
    public void testGetParcelaVariavel() {
        System.out.println("getParcelaVariavel");
        Atleta instance = new AtletaImpl();
        float expResult = 0.0F;
        float result = instance.getParcelaVariavel();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getFct method, of class Atleta.
     */
    @Test
    public void testGetFct() {
        System.out.println("getFct");
        Atleta instance = new AtletaImpl();
        float expResult = 0.0F;
        float result = instance.getFct();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getFcr method, of class Atleta.
     */
    @Test
    public void testGetFcr() {
        System.out.println("getFcr");
        Atleta instance = new AtletaImpl();
        float expResult = 0.0F;
        float result = instance.getFcr();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getContador method, of class Atleta.
     */
    @Test
    public void testGetContador() {
        System.out.println("getContador");
        Atleta instance = new AtletaImpl();
        int expResult = 0;
        int result = instance.getContador();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getValorQntPremiosMensal method, of class Atleta.
     */
    @Test
    public void testGetValorQntPremiosMensal() {
        System.out.println("getValorQntPremiosMensal");
        Atleta instance = new AtletaImpl();
        float expResult = 0.0F;
        float result = instance.getValorQntPremiosMensal();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getPercentagemITCapacidadeRespiratoria method, of class Atleta.
     */
    @Test
    public void testGetPercentagemITCapacidadeRespiratoria() {
        System.out.println("getPercentagemITCapacidadeRespiratoria");
        double expResult = 0.0;
        double result = Atleta.getPercentagemITCapacidadeRespiratoria();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getPercentagemITQueimaGordura method, of class Atleta.
     */
    @Test
    public void testGetPercentagemITQueimaGordura() {
        System.out.println("getPercentagemITQueimaGordura");
        double expResult = 0.0;
        double result = Atleta.getPercentagemITQueimaGordura();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setContador method, of class Atleta.
     */
    @Test
    public void testSetContador() {
        System.out.println("setContador");
        int contador = 0;
        Atleta instance = new AtletaImpl();
        instance.setContador(contador);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setNome method, of class Atleta.
     */
    @Test
    public void testSetNome() {
        System.out.println("setNome");
        String nome = "";
        Atleta instance = new AtletaImpl();
        instance.setNome(nome);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setNumID method, of class Atleta.
     */
    @Test
    public void testSetNumID() {
        System.out.println("setNumID");
        String numID = "";
        Atleta instance = new AtletaImpl();
        instance.setNumID(numID);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setGenero method, of class Atleta.
     */
    @Test
    public void testSetGenero() {
        System.out.println("setGenero");
        String genero = "";
        Atleta instance = new AtletaImpl();
        instance.setGenero(genero);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setIdade method, of class Atleta.
     */
    @Test
    public void testSetIdade() {
        System.out.println("setIdade");
        int idade = 0;
        Atleta instance = new AtletaImpl();
        instance.setIdade(idade);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAtividade method, of class Atleta.
     */
    @Test
    public void testSetAtividade() {
        System.out.println("setAtividade");
        String atividade = "";
        Atleta instance = new AtletaImpl();
        instance.setAtividade(atividade);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setParcelaFixa method, of class Atleta.
     */
    @Test
    public void testSetParcelaFixa() {
        System.out.println("setParcelaFixa");
        float parcelaFixa = 0.0F;
        Atleta instance = new AtletaImpl();
        instance.setParcelaFixa(parcelaFixa);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setParcelaVariavel method, of class Atleta.
     */
    @Test
    public void testSetParcelaVariavel() {
        System.out.println("setParcelaVariavel");
        float parcelaVariavel = 0.0F;
        Atleta instance = new AtletaImpl();
        instance.setParcelaVariavel(parcelaVariavel);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setFct method, of class Atleta.
     */
    @Test
    public void testSetFct() {
        System.out.println("setFct");
        float fct = 0.0F;
        Atleta instance = new AtletaImpl();
        instance.setFct(fct);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setFcr method, of class Atleta.
     */
    @Test
    public void testSetFcr() {
        System.out.println("setFcr");
        float fct = 0.0F;
        Atleta instance = new AtletaImpl();
        instance.setFcr(fct);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setPercentagemIT method, of class Atleta.
     */
    @Test
    public void testSetPercentagemIT() {
        System.out.println("setPercentagemIT");
        float percentagemIT = 0.0F;
        Atleta instance = new AtletaImpl();
        instance.setPercentagemIT(percentagemIT);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setValorQntPremiosMensal method, of class Atleta.
     */
    @Test
    public void testSetValorQntPremiosMensal() {
        System.out.println("setValorQntPremiosMensal");
        float valorQntPremiosMensal = 0.0F;
        Atleta instance = new AtletaImpl();
        instance.setValorQntPremiosMensal(valorQntPremiosMensal);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
}
