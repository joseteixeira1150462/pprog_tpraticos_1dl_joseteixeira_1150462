package clubedesportivo;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author Jose Paulo da Silva Teixeira 1150462
 */
public class AtletaAmadorIT {

    public AtletaAmadorIT() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of toString method, of class AtletaAmador.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        AtletaAmador instance = new AtletaAmador("Francisca", "623456", "Feminino", 23, "Corrida", 14, 15, 0.21f, 30, 0, 22f, 2);
        String expResult = instance.toString();
        String result = expResult;
        assertEquals(expResult, result);

    }

}
